<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <title>Ultimate Blocks</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <?php include('meta.php'); ?>

    <!-- Loading General Styles -->
   <!--  <link href="css/style.css" rel="stylesheet">
    <link href="css/style-services.css" rel="stylesheet">
    <link href="css/style-navigation.css" rel="stylesheet">
    <link href="css/style-headers.css" rel="stylesheet">
    <link href="css/style-content.css" rel="stylesheet">
    <link href="css/style-extra-pages.css" rel="stylesheet">
    <link href="css/style-basic.css" rel="stylesheet">
    <link href="css/style-team.css" rel="stylesheet">
    <link href="css/style-intro.css" rel="stylesheet">
    <link href="css/style-divider.css" rel="stylesheet">
    <link href="css/style-download.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet"> -->

    <!-- Font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <!-- Ionic -->
    <link href="css/ionicons.min.css" rel="stylesheet">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400" rel="stylesheet" type="text/css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        h1{
            text-align: center;
        }
        p{
            font-size: 16px;
            color: #000;
            font-weight: 400;
            padding: 10px 25px;
        }
    </style>
</head>
<body class=" hasGoogleVoiceExt">

    <div id="page" class="page">


        <?php include('nav.php'); ?>

        <div id="intro2">
            <div class="container">
                <div class="row">
                        <h1>About US</h1>
                        <p>Welcome to <a href="http://www.betfy.co.uk" target="_blank">betfy.co.uk</a> – the only place you need to know about for in-depth reviews and
                        ratings on betting sites in the UK. We are on a mission to cut through the clutter, look past the
                        marketing gimmicks, and give you the best possible information on all of the most popular
                        gambling websites.</p>
                        <p>We are very much focused on customers in the United Kingdom who are looking for new sites to
                        place their wagers and play online casino games. The reviews we are doing should hopefully be
                        helpful for customers across Europe and beyond, but we will always favour our friends in the UK
                        above all else.</p>
                        <p>There is no such thing as too much information as far as we&#39;re concerned. We don&#39;t do short and
                        fluffy reviews that barely tell you anything about each website – quality is the key, and customer
                        satisfaction is the goal.</p>
                        <p>Whether you are a seasoned professional or a newcomer just dipping your feet in to the waters of
                        online betting for the first time, this website is made just for you. If something doesn&#39;t look right, or
                        we think there are better deals to be found, we&#39;re going to let you know about it.</p>
                        <p>There are no bookmakers too big to escape our wrath. If they are in to shady business practices or
                        have a habit of sneaky salesmanship – we are going to investigate it to make sure our loyal visitors
                        do not fall victim to scams or malpractice.</p>
                        <p>On the other hand, we will always be on the lookout for new opportunities – big and small. If we
                        think it is important or interesting to the average sports fan or gambling enthusiast then we will do
                        our best to cover it in as much detail as humanly possible.</p>
                        <p>So, stick around for a while. Grab a beverage – stick that leftover slice of pizza in the microwave –
                        and get ready for some of the most honest and in-depth reviews that you will find anywhere online.</p>
                                
                </div>
            </div>
        </div>
         
            
    </div><!-- /#page -->


    <!-- Load JS -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.easy-pie-chart.js"></script>


</body></html>
