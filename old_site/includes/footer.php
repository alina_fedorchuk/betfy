
    <section id="footer1"><!-- USE THE BADGES https://freebets.uk/ -->
      <div class="container">
        <div class="col-md-3 col-6-xs">
           <h3>Betting app reviews</h3>
           <p><i class="fa fa-plus"></i><a href="#">Bet365</a></p>
           <p><i class="fa fa-plus"></i><a href="#">Coral</a></p>
           <p><i class="fa fa-plus"></i><a href="#">Betvictor</a></p>
        </div>

        <div class="col-md-3 col-6-xs">
           <h3 style="outline: none; cursor: inherit; outline-offset: -2px;">Address</h3>
           <p class="mt20 "><i class="fa fa-map-marker"></i>Huntington, NY 11743</p>
           <p><i class="fa fa-phone"></i><span>Phone: <span id="gc-number-0" class="gc-cs-link" title="Call with Google Voice">+1 331-331-3331</span> </span></p>
           <p><i class="fa fa-envelope-o"></i>sales@yourcompany.com</p>

       </div>

        <div class="col-md-3 col-6-xs">
            <h3 style="outline: none; cursor: inherit; outline-offset: -2px;">Social Media</h3>
            <ul class="social">
              <li class="facebook"><a href="https://www.facebook.com/Betfy-1728685757397561/" target="_blank"><i class="fa fa-facebook-official fa-5x" aria-hidden="true"></i></a></li>
              <li class="twitter"><a href="https://twitter.com/Betfy4" target="_blank"><i class="fa fa-twitter-square fa-5x" aria-hidden="true"></i></a></li>
              <li class="google"><a href="https://business.google.com/b/117246444509367718792/dashboard" target="_blank"><i class="fa fa-google-plus-square fa-5x" aria-hidden="true"></i></a></li>
            </ul>
         </div>

       <div class="col-md-3 col-6-xs">
           <h3>Responsible Gamning</h3>
           <div class="gaming_flex">
            <a href="https://validateuk.co.uk/" target="_blank" alt="Official UK ID Card" title="Official UK ID Card"><img class="rounded-shadow" src="http://www.betfy.co.uk/images/logos/over18-logo.png" alt="over18 gambling" title="over18 gambling"></a>
            <a href="http://www.gamblersanonymous.org/ga/" target="_blank"><img class="rounded-shadow" src="http://www.betfy.co.uk/images/logos/ga-logo.png" alt="Gamblers Anonymous" title="Gamblers Anonymous"></a>
            <a href="http://www.gambleaware.co.uk/" target="_blank"><img class="rounded-shadow" src="http://www.betfy.co.uk/images/logos/gambling-aware-logo.png" alt="GambleAware" title="GambleAware - Please gamble responsibly"></a>
            <a href="http://www.gamblingcommission.gov.uk/Home.aspx" target="_blank"><img class="rounded-shadow" src="http://www.betfy.co.uk/images/logos/gc-logo.png" alt="Gambling Comission" title="Gambling Comission"></a>
            <a href="https://www.gamblingtherapy.org/" target="_blank"><img class="rounded-shadow" src="http://www.betfy.co.uk/images/logos/gt-logo.png" alt="Gambling Therapy" title="Gambling Therapy"></a>
            <a href="http://www.ibas-uk.com/" target="_blank"><img class="rounded-shadow" src="http://www.betfy.co.uk/images/logos/ibas-logo.png" alt="IBAS" title="IBAS"></a>
          </div>
       </div>
   </div>
</section> <!-- /#footer1 -->

<section id="footer-rights1">
  <div class="container ">
<hr>
<div class="disclaimer_wrap">
    <h4 class=""><b>DISCLAIMER</b></h4>

<p>
  ALL FREE BETS, PROMOTIONS, AND BONUSES LISTED ARE SUBJECT TO THE TERMS AND THE INDIVIDUAL STAKING REQUIREMENTS OF THEIR RESPECTIVE OPERATORS. <a href="http://betify.co.uk/" target="_blank">betify.co.uk</a> DOES NOT FULFILL NOR PROVIDE ANY FORM OF CUSTOMER SUPPORT FOR ANY INCENTIVES THAT MAY APPEAR ON THIS SITE.</p>

<p>
  THIS SITE AND THE OFFERS WE LIST INVOLVE GAMBLING AND ARE ONLY SUITABLE FOR USERS OF LEGAL AGE IN JURISDICATIONS WHERE ONLINE GAMBLING IS PERMITTED.</p>
<p>
  ALL THIRD PARTY TRADEMARKS, LOGOS, AND IMAGES ARE THE PROPERTY OF THEIR RESPECTIVE OWNERS. TRADEMARKS AND REFERENCES TO TRADEMARKS ARE MADE UNDER THE INTENTION OF FAIR USE IN THE BUSINESS OF PROMOTING THE SERVICES OF BETTING OPERATORS.</p>
</div>
  <div class="cookies_wrap">

        <h4><b>COOKIES</b></h4>

  <p><a href="http://betfy.co.uk/" target="_blank">betfy.co.uk</a> USES FIRST AND THIRD PARTY COOKIES ON YOUR COMPUTER TO ENHANCE THIS SITE AND PROVIDE FUNCTIONALITY. BY CONTINUING TO USE OUT SITE WE WILL ASSUME YOU ACCEPT OUR USE OF COOKIES.</p>


     <p>Copyrights © 2015 All Rights Reserved by betfy.co.uk</p></div>
 </div>
</section>