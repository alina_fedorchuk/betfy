<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <title>Ultimate Blocks</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <?php include('meta.php'); ?>

    <!-- Loading General Styles -->
   <!--  <link href="css/style.css" rel="stylesheet">
    <link href="css/style-services.css" rel="stylesheet">
    <link href="css/style-navigation.css" rel="stylesheet">
    <link href="css/style-headers.css" rel="stylesheet">
    <link href="css/style-content.css" rel="stylesheet">
    <link href="css/style-extra-pages.css" rel="stylesheet">
    <link href="css/style-basic.css" rel="stylesheet">
    <link href="css/style-team.css" rel="stylesheet">
    <link href="css/style-intro.css" rel="stylesheet">
    <link href="css/style-divider.css" rel="stylesheet">
    <link href="css/style-download.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet"> -->

    <!-- Font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <!-- Ionic -->
    <link href="css/ionicons.min.css" rel="stylesheet">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400" rel="stylesheet" type="text/css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        h1, h2{
            text-align: center;
            color: #fff;
        }
        p{
            font-size: 16px;
            color: #000;
            font-weight: 400;
            padding: 10px 25px;
        }
    </style>
</head>
<body class=" hasGoogleVoiceExt">

    <div id="page" class="page">


        <?php include('nav.php'); ?>

        <div id="intro2">
            <div class="container">
                <div class="row">
                        <h1>Privacy and Cookie Policy</h1>
                        <p>We will do everything within our power to respect and maintain the privacy of you and any
                        information you decide to transmit through the website. You are not required to disclose any of your
                        personal information in order to freely browse this site.</p>
                        <p>If you do decide to submit personal information, such as your name and / or email address, it will
                        not be sold to or shared with any outside marketing agencies or similar organisations. Your details
                        will only be used for the purpose that you submitted them, such as comments, ratings, and any other
                        applicable features of the website that you decide to use.</p>
                        <h2>Cookies</h2>
                        <p>Small pieces of code, known as cookies, are used on this website to help us better identify how the
                        website is being used. These cookies do not contain personally identifiable information about you.</p>
                        <p>For example: Google Analytics is used on this website in order to measure various aspects of our
                        users and their browsing patterns within the site. This allows us to keep track of how many visitors
                        per day are viewing the website, how many pages they are viewing per visit, the country in which
                        they are visiting from, what type of web browser they are using, how they got here, and so on.</p>
                        <p>This website may use cookies to facilitate the use of other services as well. These other services
                        may include, but are not limited to: advertising, YouTube videos, customer ratings and / or review
                        plugins, and other features useful to both our visitors and the administration team.</p>
                        <p>It is important to remember that your personal information is not being collected or stored. Your
                        name, address, and other sensitive information cannot be viewed or tracked in any way through this
                        process.</p>
                        <p>You can find out more about how this works, and in-depth information about <a href="https://en.wikipedia.org/wiki/HTTP_cookie" target="_blank" alt="cookies" title="cookies">cookies on the
                        Wikipedia page or elsewhere.</a></p>
                   
                </div>
            </div>
        </div>
        
    </div><!-- /#page -->


    <!-- Load JS -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.easy-pie-chart.js"></script>


</body></html>
