    <div id="navigation">

        <nav class="navbar navbar-default">
            <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="../images/uploads/betfy_logo.png" alt="Logo outline-offset: -2px; border-radius: 0px; border: 1px none rgb(0, 255, 255);"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="https://www.betfy.co.uk/">Home</a></li>
                    <li><a href="/includes/aboutUs.php" target="_blank">About Us</a></li>
                    <li><a href="/includes/privacy.php" target="_blank">Privacy Policy</a></li>
                    <li><a href="http://www.betfy.co.uk/blog/" target="_blank">Blog</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>

        </div> <!-- /#navigation3 -->
